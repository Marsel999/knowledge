package ru.Gazizov.ДЗ_17_Library;

import java.io.*;

public class App_Library {
    public static void main(String[] args) throws IOException {
        Library library = new Library();

        Book book1 = new Book("Философия Java", "Брюс Эккель", 2000);
        library.addBook(book1);
        Book book2 = new Book("Java 8. Руководство для начинающих", "Герберт Шилдт", 2002);
        library.addBook(book2);
        Book book3 = new Book("Чистый код: создание, анализ и рефакторинг", "Роберт К.Мартин", 2009);
        library.addBook(book3);
        Book book4 = new Book("Java Concurrency in Practice", "Брайан Готц, Тим Пайерлс, Джошуа Блох", 2010);
        library.addBook(book4);

        library.listBooks();
        saveLibrary(library);
        loadLibrary(library);
    }

    private static void saveLibrary(Library library) {
        try (FileOutputStream os = new FileOutputStream("Library.txt");
             ObjectOutputStream oos = new ObjectOutputStream(os)) {
            oos.writeObject(library);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Library loadLibrary(Library library) {
        Library result = null;
        if (new File("Library.txt").exists()) {
            try (FileInputStream is = new FileInputStream("Library.txt");
                 ObjectInputStream ois = new ObjectInputStream(is)) {
                result = (Library) ois.readObject();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println(e.getMessage());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
