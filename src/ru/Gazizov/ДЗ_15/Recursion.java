package ru.Gazizov.ДЗ_15;

import java.io.File;

public class Recursion {
    static void recursive(File dir, int deep) {
        File[] files = dir.listFiles();
        for (File file : files) {
            if (file.isFile()) {
                System.out.printf(file.getAbsolutePath());
                file.delete();
                System.out.println("файл <" + file + "> удален");
            } else {
                recursive(file, deep+1); // глубина рекурсии
            }
        }
    }
}

