package ru.Gazizov.ДЗ_15;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ДЗ_15_App {
    public static void main(String[] args) throws IOException {
        System.out.printf(" === Создаю иерархию каталогов и файлов === \r\n");
        new File("dir/a/b/1.csv").mkdirs();
        new File("dir/d.txt").createNewFile();
        new File("dir/a/e.csv").createNewFile();
        new File("dir/f.txt").createNewFile();
        new File("dir/a/b/2.csv").createNewFile();
        new File("dir/a/b/3.csv").createNewFile();

        File dir = new File("dir");
        System.out.printf(" === Переименование директории === \r\n");
        dir.renameTo(new File("directory"));
        System.out.printf(" === Копирую файл === \r\n");
        try  {
            InputStream is = new FileInputStream(new File("directory/d.txt"));
        } catch (Exception E){
            System.out.printf("Не удалось скопировать файл \r\n");
            E.printStackTrace();
        }
        System.out.printf(" === Удаление файлов === ");
        Recursion.recursive(new File("directory"),0); // глубина рекурсии
        System.out.printf(" === Файлы удалены === ");
    }
}
