package ru.Gazizov.ДЗ_18_Кодировка;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Encoder {

    public static void main(String[] args) throws IOException {

        Path path = Paths.get("windows1251.txt");
        try (OutputStream os = new FileOutputStream(path.toFile())) {
            String s = "Привет, Мир!";
            os.write(s.getBytes("Windows-1251"));
        }

        try (InputStream is = new FileInputStream("windows1251.txt")) {
            byte[] buf = new byte[100];
            if (is.read(buf) != -1) {
                String s = new String(buf, "Windows-1251");
                try (OutputStream os = new FileOutputStream("KOI8")) {
                    os.write(s.getBytes(Charset.defaultCharset()));
                    is.close();
                }
            }
        }
    }
}
