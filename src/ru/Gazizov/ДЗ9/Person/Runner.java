package ru.Gazizov.ДЗ9.Person;

import java.util.Scanner;

public class Runner extends Person {
        Scanner scanner = new Scanner(System.in);
        @Override
        public void run() {
            System.out.println("Дай команду Start чтобы runner " + getName()+ " побежал");
            String start = scanner.nextLine();
            System.out.println("Задай скорость спортсмену");
            int speed = scanner.nextInt();
            System.out.println("Бегу со скоростью " + speed + "км/ч");
        }
        @Override
        public void swim() {
            System.out.println("Я не умею плавать !!!");
        }
        @Override
        public void acceleration() {
            System.out.println("Задай ускорение, км/ч : ");
            for (int speed = scanner.nextInt(); speed < 30; speed++) {
                System.out.println(speed++ + " км/ч ");
            }

        }
}