package ru.Gazizov.ДЗ9;

public class App {

    public static void main(String[] args) {
        Duck duck = new Duck();
        System.out.println(duck);
        duck.fly();
        duck.swim();
        Fish fish = new Fish();
        System.out.println(fish);
        fish.swim();
        Hare hare = new Hare();
        System.out.println(hare);
        hare.run();
    }
}
