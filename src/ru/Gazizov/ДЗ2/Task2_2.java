package ru.Gazizov.ДЗ2;

import java.util.Scanner;

import static java.lang.System.*;

public class Task2_2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(in);
        boolean Exit = false;
        while (true) {
            out.print("Enter the number or exit : ");
            while (true) {
                String tmp = scan.next();
                if ("Exit".equals(tmp)) {
                    Exit = true;
                    break;
                }
                try {
                    Integer num = Integer.valueOf(tmp);
                    break;
                } catch (NumberFormatException Ex) {
                    out.print("Not correct input! Enter the number or exit : ");
                }
                }
                if (Exit)break;
                double number = scan.nextDouble();
                if (number > 0) {
                    out.println("The number is positive");
                }
                if (number < 0) {
                    out.println("The number is negative");
                }
                if (number == 0) {
                    out.println("You entered a zero");
                }
        }
    }
}

