package ru.Gazizov.ДЗ_21_Comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<PersonSimple> peopleSimple = new ArrayList<>();
        peopleSimple.add(new PersonSimple("Mike",45));
        peopleSimple.add(new PersonSimple("Mike",25));
        peopleSimple.add(new PersonSimple("Tom",35));
        peopleSimple.add(new PersonSimple("Alexander",50));
        peopleSimple.add(new PersonSimple("Nick",40));
        peopleSimple.add(new PersonSimple("Alice",20));
        peopleSimple.add(new PersonSimple("Marsel",34));

        List<Person> people = new ArrayList<>();
        people.add(new Person("Mike", 40));
        people.add(new Person("Mike", 20));
        people.add(new Person("Tom", 35));
        people.add(new Person("Alexander",45));
        people.add(new Person("Nick",40));
        people.add(new Person("Alice",20));
        people.add(new Person("Marsel",34));

        iterate(people);

        System.out.println();
        System.out.println("peopleSimple list");
        System.out.println();

        for (PersonSimple personSimple : peopleSimple) {
            System.out.println(personSimple.getName() + " " + personSimple.getAge());
        }

        System.out.println();
        System.out.println("People list");
        System.out.println();

        for (Person person : people) {
            System.out.println(person.getName() + " " + person.getAge());
        }

        Collections.sort(people);
        Collections.sort(peopleSimple, new PersonSimpleComparator());

        System.out.println();
        System.out.println("Sorted people list");
        System.out.println();

        for (Person person : people) {
            System.out.println(person.getName() + " " + person.getAge());
        }
        System.out.println();
        System.out.println("Sorted peopleSimple list");
        System.out.println();

        for (PersonSimple person : peopleSimple) {
            System.out.println(person.getName() + " " + person.getAge());
        }
    }
    private static void iterate(List<Person> people) {
        Iterator i = people.iterator();
        while (i.hasNext()) {
            System.out.print(i.next());
            System.out.print(" : ");
        }
        System.out.println();
    }
}
