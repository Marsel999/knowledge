package ru.Gazizov.ДЗ_21_Comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class PersonSuperComparator implements Comparator {
    public static void main(String[] args) {

        ArrayList<Employee> aListEmployee = new ArrayList<Employee>();

        aListEmployee.add(new Employee("Mark", "Marketing", 45));
        aListEmployee.add(new Employee("Konstantin", "Customer Care", 24));
        aListEmployee.add(new Employee("Mike", "Public Relations", 37));
        aListEmployee.add(new Employee("Marsel", "IT", 22));

        Collections.sort(aListEmployee, new EmployeeComparator());

        System.out.println(aListEmployee);
    }

    @Override
    public int compare(Object o1, Object o2) {
        return 0;
    }

    @Override
    public Comparator reversed() {
        return null;
    }
}

class EmployeeComparator implements Comparator<Employee> {

    public int compare(Employee emp1, Employee emp2) {

        if (emp1.getAge() > emp2.getAge()) {
            return 1;
        } else if (emp1.getAge() < emp2.getAge()) {
            return -1;
        } else {
            return 0;
        }
    }
}
