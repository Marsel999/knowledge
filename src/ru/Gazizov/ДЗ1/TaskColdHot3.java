package ru.Gazizov.ДЗ1;

import java.util.Scanner;

public class TaskColdHot3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int number = (int) (Math.random() * 100 + 1);
        int numberUser = 0;
        int numberUser2;
        boolean isExit = false;

        while (true) {
            System.out.println("Отгадай число от 1 до 100 : ");
            while (scan.hasNext()) {
                String tmp = scan.next();
                if ("exit".equals(tmp)) {
                    isExit = true;
                    break;
                }
                try {
                    numberUser = Integer.valueOf(tmp);
                    break;
                } catch (NumberFormatException Ex) {
                    System.out.print("Некоректный ввод ! Введите число от 1 до 100 или exit");
                }
                if (isExit) break;
                while (numberUser != number) {
                    numberUser = scan.nextInt();
                    System.out.print("Холодно");
                    numberUser2 = scan.nextInt();
                        int diff1 = number - numberUser;
                        int diff2 = number - numberUser2;
                        if (diff2 > diff1) {
                            System.out.println("Холодно !");
                        }
                        if (diff2 < diff1) {
                            System.out.println("Горячо !");
                        }
                }
                System.out.println("Поздравляю ! Ты отгадал число " + number);
                break;
            }
        }
    }
}