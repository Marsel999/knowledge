package ru.Gazizov.Map_basket;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Basket_Map implements Metods_of_basket {
    Map<String, Integer> map = new HashMap();

    public static void main(String[] args) {
        Basket_Map basket_map = new Basket_Map();
        basket_map.addProduct("Motor Oil", Integer.parseInt("2"));
        basket_map.addProduct("Fuel filter", Integer.parseInt("2"));
        basket_map.addProduct("Oil filter", Integer.parseInt("3"));
        System.out.println(basket_map.map);
        basket_map.updateProductQuantity("Fuel filter", 5);
        System.out.println(basket_map.map);
        basket_map.getProductQuantity("Fuel filter");
    }

    @Override
    public void addProduct(String product, int quantity) {
        map.put(product, quantity);
    }


    @Override
    public void removeProduct(String product) {
        Map<String, Integer> mapCopy = new HashMap(map);
        for (Map.Entry<String, Integer> entry : mapCopy.entrySet()) {
            if (entry.getKey().equals(product))
                map.remove(entry.getKey());
        }
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (entry.getKey().equals(product))
                map.put(product, quantity);
        }
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public List<String> getProducts() {

        for (Map.Entry<String, Integer> entry : map.entrySet()){
            return Collections.singletonList(entry.getKey());
        }
        return null;
    }

    @Override
    public int getProductQuantity(String product) {
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (entry.getKey().equals(product))
                return entry.getValue();
        }
        return 0;
    }
}
