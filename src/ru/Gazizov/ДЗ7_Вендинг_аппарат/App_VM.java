package ru.Gazizov.ДЗ7_Вендинг_аппарат;

import java.util.Scanner;
import java.util.logging.Logger;

public class App_VM {
    private static final Logger logger = Logger.getLogger(String.valueOf(Log4jDemo.class));
    public static void main(String[] args) throws Exception {
        VendingMaсhine VM = new VendingMaсhine(new Drink[]{Drink.TEA, Drink.COFFEE, Drink.JUIСE});
        System.out.println(VM.getInstruction());
        System.out.println(" ");
        System.out.println(VM.getCommand1());
        Scanner scanner = new Scanner(System.in);
        switch (scanner.nextInt()) {
            case 0:
                System.out.println(VM.getMenu());
                logger.info("Вызван метод : " + VM.getMenu());
            case 9:
                System.out.println(VM.getCommand3());
                VM.setMoney(scanner.nextInt());
                if (VM.getMoney() >= 15) {
                    System.out.println(VM.getCommand4());
                    System.out.println(VM.getChooseDrinks());
                }
                if (VM.getMoney() < 15) {
                    try {
                        throw new Exception(VM.getNoMoney());
                    } catch (Exception E) {
                        E.printStackTrace();
                        logger.warning("НЕДОСТАТОЧНО СРЕДСТВ !");
                    } finally {
                        System.out.println(VM.getMoney() + VM.getRub());
                    } break;
                }
                switch (scanner.nextInt()) {
                    case 1:
                        if (VM.getMoney() >= 15) {
                            System.out.println(VM.getTea());
                            logger.info("Вызван метод : " + VM.getTea());
                            System.out.println(VM.getMoney() - 15 + VM.getRub());
                        }
                        if (VM.getMoney() < 15) {
                            try {
                                throw new Exception(VM.getNoMoney());
                            } catch (Exception E) {
                                E.printStackTrace();
                                logger.warning("НЕДОСТАТОЧНО СРЕДСТВ !");
                            } finally {
                                System.out.println(VM.getMoney() + VM.getRub());
                            }
                        }
                        break;
                    case 2:
                        if (VM.getMoney() >= 50) {
                            System.out.println(VM.getCoffee());
                            logger.info("Вызван метод : " + VM.getCoffee());
                            System.out.println(VM.getMoney() - 50 + VM.getRub());
                        }
                        if (VM.getMoney() < 50) {
                            try {
                                throw new Exception(VM.getNoMoney());
                            } catch (Exception E) {
                                E.printStackTrace();
                                logger.warning("НЕДОСТАТОЧНО СРЕДСТВ !");
                            } finally {
                                System.out.println(VM.getMoney() + VM.getRub());
                            }
                        }
                        break;
                    case 3:
                        if (VM.getMoney() >= 40) {
                            System.out.println(VM.getJuice());
                            logger.info("Вызван метод : " + VM.getJuice());
                            System.out.println(VM.getMoney() - 40 + VM.getRub());
                        }
                        if (VM.getMoney() < 40) {
                            try {
                                throw new Exception(VM.getNoMoney());
                            } catch (Exception E) {
                                E.printStackTrace();
                                logger.warning("НЕДОСТАТОЧНО СРЕДСТВ !");
                            } finally {
                                System.out.println(VM.getMoney() + VM.getRub());
                            }
                        }
                        break;
                    case 0:
                        System.out.println(VM.getMenu());
                        logger.info("Вызван метод : " + VM.getMenu());
                        break;
                    default:
                        System.out.println(VM.getDef());
                        System.out.println(VM.getMoney() + VM.getRub());
                        logger.info("Вызван метод : " + VM.getMoney() + VM.getRub());
                }
                break;
            default:
                System.out.println(VM.getRet());
                logger.info("Вызван метод : " + VM.getRet());
        }
    }
}
