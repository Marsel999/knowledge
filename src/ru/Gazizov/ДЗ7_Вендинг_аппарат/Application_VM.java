package ru.Gazizov.ДЗ7_Вендинг_аппарат;

import java.util.Scanner;
import java.util.logging.Logger;

public class Application_VM {
    private static final Logger logger = Logger.getLogger(String.valueOf(Log4jDemo.class));
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        VendingMachine_new VM = new VendingMachine_new(new Drink[]{Drink.TEA, Drink.COFFEE, Drink.JUIСE});
        System.out.println(VM.getCommandMenu());
        System.out.println(VM.showMenu(scanner.nextInt()));
        System.out.println(VM.getCommand());
        VM.addMoney(scanner.nextDouble());
        logger.info("Вызван метод ");
        VM.giveMeADrink(scanner.nextInt());
        logger.info("Вызван метод ");
    }
}
