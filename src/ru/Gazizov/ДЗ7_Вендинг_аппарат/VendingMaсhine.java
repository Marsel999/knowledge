package ru.Gazizov.ДЗ7_Вендинг_аппарат;

import java.util.Arrays;

public class VendingMaсhine {
    private Drink drinks[];
    private int money;
    private int key = 8;
    private String noMoney = "НЕДОСТАТОЧНО СРЕДСТВ ! ВОЗЬМИТЕ ДЕНЬГИ : ";
    private String chooseDrinks = "[ЧАЙ - 1] [КОФЕ - 2] [СОК - 3]";
    private String menu = "Меню напитков : ";
    private String command1 = "ШАГ 1: ОЗНАКОМТЕСЬ С МЕНЮ";
    private String command2 = "ШАГ 2: ПЕРЕЙТИ К ОПЛАТЕ";
    private String command3 = "ШАГ 2: ВНЕСИТЕ ДЕНЬГИ";
    private String command4 = "ШАГ 3: ВЫБЕРИТЕ НАПИТОК";
    private String rub = " рублей ";
    private String def = "НАПИТОК ОТСУТСТВУЕТ ! ВОЗЬМИТЕ ДЕНЬГИ";
    private String ret = "НЕ КОРРЕКТНЫЙ ВВОД... ВЕРНИТЕСЬ В МЕНЮ ПРОГРАММЫ";
    public VendingMaсhine() throws Exception {
        try {
            throw new Exception(getNoMoney());
        } catch (Exception E) {
            E.printStackTrace();
        } finally {
            System.out.println(getMoney() + getRub());
        }
    }
    public String getCommand1() { return command1; }
    public String getCommand2() {
        return command2;
    }
    public String getCommand3() {
        return command3;
    }
    public String getCommand4() {
        return command4;
    }
    private String tea = Drink.TEA + " будет готов через 15 секунд";
    public String getTea() {
        return tea;
    }
    private String coffee = Drink.COFFEE + " будет готов через 20 секунд";

    public String getCoffee() {
        return coffee;
    }
    private String juice = Drink.JUIСE + " будет готов через 10 секунд";

    public String getJuice() {
        return juice;
    }

    public String getMenu() {
        return menu + Arrays.toString(drinks);
    }
    public Drink[] getDrinks() {
        return drinks;
    }
    public int getKey() {
        return key;
    }
    private String instruction = new String("Инструкция : Показать меню - 0, Перейти к оплате - 9");
    public String getInstruction() {
        return instruction;
    }
    public Drink[] getDrinks(Drink TEA) {
        return drinks;
    }
    public int getMoney() {
        return money;
    }
    public void setMoney(int money) {
        this.money = money;
    }
    public VendingMaсhine(Drink[] drinks) {
        this.drinks = drinks;
    }
    public String getChooseDrinks() { return chooseDrinks; }
    public String getNoMoney() { return noMoney; }
    public String getRub() { return rub; }
    public String getDef() { return def; }
    public String getRet() { return ret; }

    @Override
    public String toString() {
        return "VendingMaсhine{" +
                "drinks=" + Arrays.toString(drinks) +
                ", money=" + money +
                ", key=" + key +
                ", noMoney='" + noMoney + '\'' +
                ", chooseDrinks='" + chooseDrinks + '\'' +
                ", menu='" + menu + '\'' +
                ", command1='" + command1 + '\'' +
                ", command2='" + command2 + '\'' +
                ", command3='" + command3 + '\'' +
                ", command4='" + command4 + '\'' +
                ", rub='" + rub + '\'' +
                ", def='" + def + '\'' +
                ", ret='" + ret + '\'' +
                ", tea='" + tea + '\'' +
                ", coffee='" + coffee + '\'' +
                ", juice='" + juice + '\'' +
                ", instruction='" + instruction + '\'' +
                '}';
    }
}




