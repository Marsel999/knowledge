package ru.Gazizov.ДЗ7_Вендинг_аппарат;

import java.util.Arrays;

        public class VendingMachine_new {
            private Drink drinks[];
            private double money;
            private String commandMenu = "Для вызова меню нажмите клавишу";
            private static String menu = "МЕНЮ НАПИТКОВ : ";
            private String command = "ВНЕСИТЕ ДЕНЬГИ : ";

            public String getCommandMenu() {
                return commandMenu;
            }

            public String getCommand() {
                return command;
            }

            public String getMenu() {
                return menu + Arrays.toString(drinks);
            }

            public String showMenu(int key) {
                return menu + Arrays.toString(drinks);
            }

            public VendingMachine_new(Drink[] drinks) {
                this.drinks = drinks;
            }

            public void addMoney(double money) {
                this.money = money;
            }

            private Drink getDrink(int key) {
                if (key < drinks.length) {
                    return drinks[key];
                } else {
                    return null;
                }
            }

            public void giveMeADrink(int key) {
                if (this.money > 0) {
                    Drink drink = getDrink(key);
                    if (drink != null) {
                        if (drink.getCost() <= money) {
                            System.out.println("ВОЗЬМИТЕ ВАШ НАПИТОК : " + drink.getTitle());
                            System.out.println("ВОЗЬМИТЕ ВАШУ СДАЧУ : ");
                            System.out.println(money -= drink.getCost());
                        } else {
                            System.out.println("НЕДОСТАТОЧНО СРЕДСТВ!");
                        }
                    }
                } else {
                    System.out.println("ВНЕСИТЕ ДЕНЬГИ");
                }
            }
}
