package ru.Gazizov.ДЗ7_Вендинг_аппарат;

public enum Drink {
    TEA("ЧАЙ", 15),
    COFFEE("КОФЕ",50),
    JUIСE("СОК",40);

    Drink(String title, double cost) {
        this.title = title;
        this.cost = cost;
    }
    @Override
    public String toString() {
        return "Drink{" +
                "title='" + title + '\'' +
                ", cost=" + cost +
                '}';
    }
    private String title;
    private double cost;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
}





