package ru.Gazizov.ДЗ_23_Корзина_интернет_магазина;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Basket_online_store implements Basket {
    ArrayList<Product> myArrayList = new ArrayList<>();

    public static void main(String[] args) {
        Basket_online_store basket_online_store = new Basket_online_store();
        basket_online_store.addProduct("Motor oil", 2);
        basket_online_store.addProduct("Oil filter", 1);
        basket_online_store.addProduct("Fuel filter", 2);
        basket_online_store.addProduct("Fuel filter", 2);
        System.out.println(basket_online_store.myArrayList);
        System.out.println("Next ->");
        basket_online_store.updateProductQuantity("Oil filter", 2);
        System.out.println(basket_online_store.myArrayList);
        System.out.println("Next ->");
        basket_online_store.getProducts();
        System.out.println("Next ->");
        basket_online_store.getProductQuantity("Fuel filter");
    }

    @Override
    public void addProduct(String product, int quantity) {
        myArrayList.add(new Product(product, quantity));
    }

    @Override
    public void removeProduct(String product) {
        List<Product> toRemove = new ArrayList<>();
        for (Product p : myArrayList) {
            if (p.getProduct().equals(product)) toRemove.add(p);
        }
        myArrayList.removeAll(toRemove);
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
        for (Product p : myArrayList) {
            if (p.getProduct().equals(product)) p.setQuantity(quantity);
        }
    }

    @Override
    public void clear() {
        myArrayList.clear();
    }

    @Override
    public List<String> getProducts() {
        for (Product p : myArrayList) {
            return Collections.singletonList(p.getProduct());
        }
        return null;
    }

    @Override
    public int getProductQuantity(String product) {
        for (Product p : myArrayList) {
            if (p.getProduct().equals(product)) {
                return p.getQuantity();
            }
        }
        return 0;
    }
}
