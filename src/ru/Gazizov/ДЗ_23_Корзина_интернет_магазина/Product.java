package ru.Gazizov.ДЗ_23_Корзина_интернет_магазина;

public class Product {
    String product;
    int quantity;

    public Product(String product, int quantity) {
        this.product = product;
        this.quantity  = quantity;
    }

    @Override
    public String toString() {
        return "Basket1{" +
                "product='" + product + '\'' +
                ", quantity=" + quantity +
                '}';
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


}
