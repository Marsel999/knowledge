package ru.Gazizov.ДЗ_20_JSON;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.URL;

@JsonAutoDetect
public class SerializeJson implements Serializable {
    public static void main(String[] args) throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();

        try {
            URL url = new URL("https://official-joke-api.appspot.com/jokes/random");

            try (InputStream is = url.openStream()) {

                try (Reader reader = new InputStreamReader(is)) {

                    try (BufferedReader br = new BufferedReader(reader)) {

                        Joke joke = objectMapper.readValue(br.readLine(), Joke.class);
                        System.out.println(joke);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}