package ru.Gazizov.ДЗ_19_КАССОВЫЙ_ЧЕК;

import java.io.*;
import java.util.Scanner;

public class App_check {

    public static void main(String[] args) throws IOException {

        InputStream is = System.in;
        PrintStream os = System.out;
        Scanner scanner = new Scanner(is);

        float total = 0;

        while (scanner.hasNext()) {
            String name = scanner.nextLine();
            boolean isNextFloat = scanner.hasNextFloat();
            float quantity = scanner.nextFloat();
            float price = scanner.nextFloat();
            scanner.nextLine();
            if ("q".equals(name)) {
                break;
            }
            if (isNextFloat) {
                os.printf("%s\n %f\n %f = %f\n", name, quantity, price, quantity * price);
            } else {
                os.printf("%s\n %d\n %f = %f\n", name, (int) quantity, price, quantity * price);
            }
            total += quantity * price;
        }
        os.printf("total: = %f\n", total);
    }
}
