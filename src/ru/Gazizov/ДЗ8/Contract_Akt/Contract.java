package ru.Gazizov.ДЗ8.Contract_Akt;

import java.time.LocalDate;

public class Contract {
    private int number;
    private LocalDate contract;
    private String name;

    public Contract(int number, LocalDate contract, String name) {
        this.number = number;
        this.contract = contract;
        this.name = name;
    }
    public int getNumber() {
        return number;
    }
    public void setNumber(int number) {
        this.number = number;
    }
    public LocalDate getContract() {
        return contract;
    }
    public void setContract(LocalDate contract) {
        this.contract = contract;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
