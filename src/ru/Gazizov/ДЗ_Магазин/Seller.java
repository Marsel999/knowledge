package ru.Gazizov.ДЗ_Магазин;

import java.time.LocalDate;

public class Seller extends Person {
    private double salary;
    private String prise;

    public String getPrise() {
        return prise;
    }

    public void setPrise(String prise) {
        this.prise = prise;
    }

    @Override
    public String toString() {
        return "Seller{" +
                "salary=" + salary +
                ", prise=" + prise +
                ", customer=" + customer +
                '}';
    }
    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Seller() {
        super("Ivan","Ivanov",LocalDate.of(1984,10,5));

    }
    Customer customer = new Customer("Piter","Pen",LocalDate.of(1987, 5, 9));

    public static void takeMoney() {

    }
}