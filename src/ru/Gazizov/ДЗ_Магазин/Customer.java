package ru.Gazizov.ДЗ_Магазин;

import java.time.LocalDate;

public class Customer extends Person {
    private int money;

    public Customer(String firstName, String lastName, LocalDate birthday) {
        super(firstName, lastName, birthday);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "money=" + money +
                '}';
    }

    public static void giveMoney() {

    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }
}
