package ru.Gazizov.ДЗ_Магазин;

import java.util.Arrays;

public class Direction {
    private String AutoParts;
    private Seller[] sellers = new Seller[5];
    Seller seller = new Seller();

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public String getAutoParts() {
        return AutoParts;
    }

    public void setAutoParts(String autoParts) {
        AutoParts = autoParts;
    }

    public Seller[] getSellers() {
        return sellers;
    }

    void setSellers(Seller[] sellers) {
        this.sellers = sellers;
    }

    @Override
    public String toString() {
        return "Direction{" +
                "AutoParts='" + AutoParts + '\'' +
                ", sellers=" + Arrays.toString(sellers) +
                '}';
    }
}
