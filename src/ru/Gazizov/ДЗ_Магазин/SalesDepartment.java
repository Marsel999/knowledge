package ru.Gazizov.ДЗ_Магазин;

import java.util.Arrays;

public class SalesDepartment {
    private String Cars;
    private Direction[] directions = new Direction[3];
    private Direction direction = new Direction();

    @Override
    public String toString() {
        return "SalesDepartment{" +
                "Cars='" + Cars + '\'' +
                ", directions=" + Arrays.toString(directions) +
                ", direction=" + direction +
                '}';
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public String getCars() {
        return Cars;
    }

    public void setCars(String cars) {
        Cars = cars;
    }

    public Direction[] getDirections() {
        return directions;
    }

    void setDirections(Direction[] directions) {
        this.directions = directions;
    }

}
