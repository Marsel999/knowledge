package ru.Gazizov.ДЗ_Магазин;

import java.time.LocalDate;

public class StoreApp {

    public static void main(String[] args) {
        Seller ivan = new Seller();
        Customer piter = new Customer("Piter", "Pen", LocalDate.of(1987, 5, 9));

        piter.setFirstName("Piter");
        System.out.println(piter.getFirstName());
        piter.setMoney(10000);

        ivan.setFirstName("Иван");
        System.out.println(ivan.getFirstName());
        ivan.setSalary(80000);

        Direction AutoChemicalGoods = new Direction();
        SalesDepartment wholesales = new SalesDepartment();

        AutoChemicalGoods.setSellers(new Seller[]{ivan});
        wholesales.setDirections(new Direction[]{AutoChemicalGoods});

        System.out.println(wholesales);
        System.out.println();
    }
}
