package ru.Gazizov;

import java.util.Scanner;

public class TaskHotCold {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int number = (int) (Math.random() * 100 + 1);
        int numberUser = 0;
        int numberUser2;
        boolean isExit = false;

        while (! isExit) {
            System.out.println("Отгадай число от 1 до 100");
            while (scan.hasNext()){
                System.out.print("Введите число или exit : ");
                String tmp = scan.next();
                if ("exit".equals(tmp)){
                    isExit = true;
                    break;
                }
                try {
                    numberUser = Integer.valueOf(tmp);
                    break;
                } catch (NumberFormatException Ex){
                    System.out.print("Некоректный ввод ! ");
                }
            }
            if (isExit) break;
            numberUser = scan.nextInt();
            numberUser2 = scan.nextInt();
            int diff1 = number - numberUser;
            int diff2 = number - numberUser2;


            if (numberUser < number || number < numberUser) {
                System.out.println("Холодно");
            }
            if (diff1 > diff2) {
                System.out.println("Горячо");
            }
            if (diff1 < diff2) {
                System.out.println("Холодно");
            }
            {
                if (numberUser == number || numberUser2 == number)
                    System.out.println("Поздравляю ! Ты отгадал число " + number);
            }
        }
    }
}