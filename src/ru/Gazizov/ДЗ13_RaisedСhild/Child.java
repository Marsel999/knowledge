package ru.Gazizov.ДЗ13_RaisedСhild;

import java.util.Arrays;
import java.util.List;

public class Child {
    private static List<Food> myFood = Arrays.asList(Food.Apple, Food.Milk, Food.Porridge);

    public static void eat(Food food) throws Exception {
        try {
            if (myFood.contains(food)) {
                System.out.println("Спасибо, было вкусно !");
            } else {
                throw new Exception("Я не хочу это есть !");
            }
        } finally {
            System.out.println("Спасибо мама !");
        }
    }
}


