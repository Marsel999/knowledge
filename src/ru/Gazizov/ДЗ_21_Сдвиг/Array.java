package ru.Gazizov.ДЗ_21_Сдвиг;

import java.util.Arrays;

class Array {
    private long[] a;
    private int nElems;

    public Array(int max) {
        a = new long[max];
        nElems = 0;
    }

    public void insert(long value) {
        a[nElems] = value;
        nElems++;
    }

    public void toLeft(int one, int two) {
        long temp = a[one];
        a[one] = a[two];
        a[two] = temp;
    }

    @Override
    public String toString() {
        return "Array{" +
                "a=" + Arrays.toString(a) +
                ", nElems=" + nElems +
                '}';
    }
}
