package ru.Gazizov.ДЗ_21_Сдвиг;

import java.util.Arrays;

public class Array2 {
    private int nElems;
    private int nElems2;
    private long[][] b;

    public Array2(int max) {
        b = new long[max][max];
        nElems = 0;
    }

    public void insert(long value, long value1) {
        b[nElems][nElems2] = b[(int) value][(int) value1];
        nElems++;
        nElems2++;
    }

    public void toLeft(int one, int two, int three, int four) {
        long temp = b[one][two];
        b[one][two] = b[three][four];
        b[three][four] = temp;
    }

    @Override
    public String toString() {
        return "Array2{" +
                "nElems=" + nElems +
                ", nElems2=" + nElems2 +
                ", b=" + Arrays.toString(b) +
                '}';
    }
}
