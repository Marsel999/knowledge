package ru.Gazizov.Реверс_массива;

public class Revers {
    public static void main(String[] args) {
        int[] a;
        a = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        for (int value : a) {
            System.out.println(value);
        }
        int n = a.length;
        int temp;

        for (int i = 0; i < n / 2; i++) {
            temp = a[n - i - 1];
            a[n - i - 1] = a[i];
            a[i] = temp;
        }

        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }
    }
}