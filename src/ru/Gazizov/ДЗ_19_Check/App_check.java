package ru.Gazizov.ДЗ_19_Check;

import java.io.*;
import java.util.Formatter;
import java.util.Scanner;

public class App_check {

    public static void main(String[] args) throws IOException {

        try (FileReader fileReader = new FileReader("src\\ru\\Gazizov\\products.txt")) {

            try (FileOutputStream fileOutputStream = new FileOutputStream(new File("src\\ru\\Gazizov\\out(1).txt"))) {

                try (OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream)) {
                    Scanner scanner = new Scanner(fileReader);
                    Formatter formatter = new Formatter();

                    float total = 0;

                    outputStreamWriter.write(printReport());

                    while (scanner.hasNext()) {

                        String name = scanner.nextLine();
                        boolean isNextFloat = scanner.hasNextFloat();
                        float quantity = Float.parseFloat(scanner.nextLine());
                        float price = Float.parseFloat(scanner.nextLine());

                        if ("q".equals(name)) {
                            break;
                        }
                        if (isNextFloat) {
                            formatter.format("%.16s %,.2f х %f = %10f\n", name, quantity, price, quantity * price);
                            outputStreamWriter.write(formatter.toString());
                        } else {
                            formatter.format("%.16s %,.2g х %f = %10f\n", name, quantity, price, quantity * price);
                            outputStreamWriter.write(formatter.toString());
                        }
                        total += quantity * price;
                    }
                    formatter.format("Итого:                          =%10.2f", total);
                    outputStreamWriter.write(formatter.toString());
                    System.out.printf("Итого: = %f", total);
                }
            } catch (FileNotFoundException e) {
                System.out.println("Файл не был найден");
                e.getMessage();
                e.printStackTrace();
            }
        }

    }

    private static String printReport() {
        String report = " Наименование    Кол-во    Цена    Стоимость\n============================================\n";
        return report;
    }
}

